``` bash
cd ~
git clone --recurse-submodules https://gitlab.com/hongnguyenhuu96/dotfiles.git ~/.dotfiles

curl -fLo ~/.dotfiles/vim/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

chmod 777 ~/.dotfiles/install_plugins_theme_zsh.sh && ~/.dotfiles/install_plugins_theme_zsh.sh
cd .dotfiles && stow vim zsh tmux
chsh -s $(which zsh)
```


## Fonts
Linux
``` bash
cp -r ~/.dotfiles/fonts ~/.local/share/fonts
fc-cache -f -v
```

OSX
``` bash
cp -r ~/.dotfiles/fonts ~/Library/Fonts
```

To install plugins for vim, open vim in visual mode `:PlugInstall`
